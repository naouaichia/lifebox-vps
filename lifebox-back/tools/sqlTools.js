const sqlite3 = require('sqlite3').verbose();


var debugAndSecur = require('./debugAndSecur');

module.exports = {
  createUnikId: function (e) {
    try {
      var text = "";
      var possible = "abcdefghijklmnopqrstuvwxyz";

      for( var i=0; i < 5; i++ )
          text += possible.charAt(Math.floor(Math.random() * possible.length));

      var rightNow = new Date();
      var res = rightNow.toISOString().slice(0,10).replace(/-/g,"");

      text += res;

      text += rightNow.getHours();
      text += rightNow.getMinutes();
      text += rightNow.getSeconds();
      return text;
    } catch (e) {
      debugAndSecur.saveError('createUnikId')
      debugAndSecur.saveError(e)
      console.log(e);
    }
  },
  closeDB: function (db) {
    try {
      db.close();
      return true;
    } catch (e) {
      debugAndSecur.saveError('closeDB')
      debugAndSecur.saveError(e)
      console.log(e);
    }

  },
  openDB: function (dbPath) {
    try {
      var db = new sqlite3.Database(dbPath);
      db.on("error", function(error) {
        console.log("SQLSQLSQL ERROR");
        return false
      });
      return db;
    } catch (e) {
      debugAndSecur.saveError('openDB')
      debugAndSecur.saveError(e)
      console.log(e);
    }
  }
};
