const fs  = require('fs');

module.exports = {
  saveError: function (e) {
    try {
      var dateObj = new Date();
      var errorDate = dateObj.toString();
      fs.appendFile('lifebox-vps-error-history.txt', '<br>'+errorDate+'-->'+e, function (err) {
        if (err) throw err;
        console.log('Saved!');
      });
    } catch (e) {
      console.log('DEBUG PROBLEME');
      console.log(e);
    }
  },
  getError: function (res) {
    try {
      fs.readFile('lifebox-vps-error-history.txt', 'utf8', function (err,data) {
        if (err) {
          res.send('impossible de lire le fichier log d\'errreur'+err);
        }
          res.send('<h1>Erreurs survenue sur lifebox-vps</h1><br><br>'+data);

      });
    } catch (e) {
      console.log('DEBUG PROBLEME');
      console.log(e);
    }
  }
};
