/*
*
* CREER UNE TABLE DEDIER POUR STOCKER LES STAT SENSOR DE LA BOX
*
*/
var sqlTools = require('../tools/sqlTools');

module.exports = {
  creatStory: function (pathToDb, box , res) {
    try {

      var ActualDate = new Date().toString();
      var unikId = sqlTools.createUnikId();
      var db = sqlTools.openDB(pathToDb);
      var stmt =  db.prepare("CREATE TABLE `stats"+box+"` (`date` NUMERIC UNIQUE, `temp`	REAL, `hydro`	REAL);");
      stmt.run();
      stmt.finalize();
      sqlTools.closeDB(db);
      res.send("box created with a deied table for all story")
    } catch (e) {
      debugAndSecur.saveError('creatStory')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  }
};
