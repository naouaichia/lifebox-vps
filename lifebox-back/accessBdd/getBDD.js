/*
*
* FUNCTION DE RECUPERATION DES VALEUR DES ITEM DANS LA BDD
*
*/
var sqlTools = require('../tools/sqlTools');

var debugAndSecur = require('../tools/debugAndSecur');

module.exports = {
  getError: function (e) {
    console.log(e);
  },
  getUser: function (pathToDb, req, res) {
    try {
      var db = sqlTools.openDB(pathToDb);
      var result = db.all("SELECT * FROM lbUsers WHERE user='"+req.params.user+"' AND pass='"+req.params.pass+"'",
        function(err, table) {
          console.log(table);
          res.send(table);
      });
      sqlTools.closeDB(db)
    } catch (e) {
      debugAndSecur.saveError('getUser')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  },
  getClimat: function (pathToDb, req, res) {
    try {
      var db = sqlTools.openDB(pathToDb);
      var result = db.all("SELECT * FROM lbClimats WHERE "+req.params.getterClim+"='"+req.params.valueClim+"'",
        function(err, table) {
          console.log(table);
          res.send(table);
      });
      sqlTools.closeDB(db)

    } catch (e) {
      debugAndSecur.saveError('getClimat')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  },
  getBox: function (pathToDb, req, res) {
    try {
      var db = sqlTools.openDB(pathToDb);
      var result = db.all("SELECT * FROM lbBoxs WHERE user='"+req.params.user+"'",
        function(err, table) {
          console.log(table);
          res.send(table);
      });
      sqlTools.closeDB(db)

    } catch (e) {
      debugAndSecur.saveError('getBox')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  },
  getHistory: function (pathToDb, req, res) {
    try {
      var db = sqlTools.openDB(pathToDb);
      var result = db.all("SELECT * FROM stats"+req.params.box+" WHERE date<='"+req.params.end+"' AND date>='"+req.params.start+"'",
        function(err, table) {
          console.log(table);
          res.send(table);
      });
      sqlTools.closeDB(db)

    } catch (e) {
      debugAndSecur.saveError('getHistory')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  }
};
