/*
*
* FUNCTION DE MODIFICATION DES VALEUR DES ITEM DANS LA BDD
*
*/
var sqlTools = require('../tools/sqlTools');

var debugAndSecur = require('../tools/debugAndSecur');

module.exports = {
  modifyError: function (e) {
    console.log(e);
  },
  modifyUser: function (pathToDb, req, res) {
    try {

      var db = sqlTools.openDB(pathToDb);

      db.run("UPDATE lbUsers SET "+req.params.prop+"='"+req.params.newValue+"' WHERE user='"+req.params.user+"' AND pass='"+req.params.pass+"'");

      sqlTools.closeDB(db)

      res.send("user modifyed")
    } catch (e) {
      debugAndSecur.saveError('modifyUser')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  },
  modifyClimat: function (pathToDb, req, res) {
    try {

      var db = sqlTools.openDB(pathToDb);

      db.run("UPDATE lbClimats SET "+req.params.prop+"='"+req.params.newValue+"' WHERE creatBy='"+req.params.user+"' AND name='"+req.params.climat+"'");

      sqlTools.closeDB(db)
      console.log("okokok");
      console.log(req.params.prop);
      console.log(req.params.newValue);
      console.log(req.params.user);
      console.log(req.params.name);
      res.send("climat modifyed")
    } catch (e) {
      debugAndSecur.saveError('modifyClimat')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  },
  modifyBox: function (pathToDb, req, res) {
    try {

      var db = sqlTools.openDB(pathToDb);

      db.run("UPDATE lbBoxs SET "+req.params.prop+"='"+req.params.newValue+"' WHERE user='"+req.params.user+"' AND id='"+req.params.box+"'");

      sqlTools.closeDB(db)

      res.send("box modifyed")
    } catch (e) {
      debugAndSecur.saveError('modifyBox')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  },
  modifyHistory: function (pathToDb, req, res) {
    try {

      var db = sqlTools.openDB(pathToDb);

      db.run("UPDATE stats"+req.params.box+" SET "+req.params.prop+"='"+req.params.newValue+"' WHERE date='"+req.params.date+"'");

      sqlTools.closeDB(db)

      res.send("history modifyed")
    } catch (e) {
      debugAndSecur.saveError('modifyHistory')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  }
};
