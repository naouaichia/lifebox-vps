/*
*
* FUNCTION D AJOUT DES ITEM DANS LA BDD
*
*/
var sqlTools = require('../tools/sqlTools');
var creatBDD = require('../tools/creatBDD');

var debugAndSecur = require('../tools/debugAndSecur');

module.exports = {
  addError: function (e) {
    console.log(e);
  },
  addUser: function (pathToDb, req, res) {
    try {

      var ActualDate = new Date().toString();
      var unikId = sqlTools.createUnikId();
      var db = sqlTools.openDB(pathToDb);
      var stmt =  db.prepare("INSERT INTO lbUsers VALUES(?, ?, ?, ?, ?, ?)");
      stmt.run(unikId, req.params.user,	req.params.pass, ActualDate,	"exemple@email.com", 0);
      stmt.finalize();
      sqlTools.closeDB(db)

      res.send("user created")
    } catch (e) {
      debugAndSecur.saveError('addUser')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  },
  addClimat: function (pathToDb, req, res) {
    try {

      `id`, `name`, `type`, `behaviour`, `latLng`, `target`, `creatBy`, `creatAt`, `statut`

      var ActualDate = new Date().toString();
      var unikId = sqlTools.createUnikId();
      var db = sqlTools.openDB(pathToDb);
      var stmt =  db.prepare("INSERT INTO lbClimats VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
      stmt.run( unikId, req.params.name, req.params.type, req.params.prog, req.params.latLng, req.params.target, req.params.user, ActualDate, req.params.statut	 );
      stmt.finalize();
      sqlTools.closeDB(db)

      res.send("climat created")
    } catch (e) {
      debugAndSecur.saveError('addClimat')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  },
  addBox: function (pathToDb, req, res) {
    try {

      var db = sqlTools.openDB(pathToDb);
      var stmt =  db.prepare("INSERT INTO lbBoxs VALUES(?, ?, ?, ?, ?, ?)");
      stmt.run( req.params.box, req.params.user, `{}`, `stats`+req.params.box, `{"order-1":"start"}`, `inconue` );
      stmt.finalize();
      sqlTools.closeDB(db)

      creatBDD.creatStory(pathToDb, req.params.box , res)

    } catch (e) {
      console.log(e)
      debugAndSecur.saveError('addBox')
      debugAndSecur.saveError(e)
      return false
    }
  },
  addHistory: function (pathToDb, req, res) {
    try {

      var db = sqlTools.openDB(pathToDb);
      var stmt =  db.prepare("INSERT INTO stats"+req.params.box+" VALUES(?, ?, ?)");
      stmt.run( req.params.date, req.params.temp, req.params.hydro);
      stmt.finalize();
      sqlTools.closeDB(db)

      res.send("history added")
    } catch (e) {
      console.log(e)
      debugAndSecur.saveError('addHistory')
      debugAndSecur.saveError(e)
      return false
    }
  }
};
