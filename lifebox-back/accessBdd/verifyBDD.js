/*
*
* FUNCTION DE RECUPERATION DES VALEUR DES ITEM DANS LA BDD
*
*/
var sqlTools = require('../tools/sqlTools');

var debugAndSecur = require('../tools/debugAndSecur');

module.exports = {
  verifyError: function (e) {
    console.log(e);
  },
  verifyUser: function (pathToDb, req, res, trueCB, falseCB) {
    try {
      var db = sqlTools.openDB(pathToDb);
      var result = db.all("SELECT * FROM lbUsers WHERE user='"+req.params.user+"' AND pass='"+req.params.pass+"'",
        function(err, table) {
          console.log(table);
          if(table.length > 0){
            trueCB(pathToDb, req, res)
          }else{
            falseCB(res)
          }
      });
      sqlTools.closeDB(db)

    } catch (e) {
      debugAndSecur.saveError('verifyUser')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  },
  verifyClimat: function (pathToDb, req, res, trueCB, falseCB) {
    try {
      var db = sqlTools.openDB(pathToDb);
      var result = db.all("SELECT * FROM lbClimats WHERE creatBy='"+req.params.user+"' AND name='"+req.params.climat+"'",
        function(err, table) {
          console.log(table);
          if(table.length > 0){
            trueCB(pathToDb, req, res)
          }else{
            falseCB(res)
          }
      });
      sqlTools.closeDB(db)

    } catch (e) {
      debugAndSecur.saveError('verifyClimat')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  },
  verifyBox: function (pathToDb, req, res, trueCB, falseCB) {
    try {
      var db = sqlTools.openDB(pathToDb);
      var result = db.all("SELECT * FROM lbBoxs WHERE user='"+req.params.user+"' AND id='"+req.params.box+"'",
        function(err, table) {
          console.log(table);
          if(table.length > 0){
            trueCB(pathToDb, req, res)
          }else{
            falseCB(res)
          }
      });
      sqlTools.closeDB(db)


    } catch (e) {
      debugAndSecur.saveError('verifyBox')
      debugAndSecur.saveError(e)
      console.log(e)
      return false
    }
  }
};
