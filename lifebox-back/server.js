/*-------------------------------------*/
/*--------------------REQUIRE----------*/
/*-------------------------------------*/
const path                  = require('path');        // call express
const express               = require('express');
const bodyParser            = require('body-parser');
const liveServer            = require("live-server");
const swig                  = require('swig');
const fs                    = require('fs');
const lineReader            = require('line-reader');
const sqlite3               = require('sqlite3').verbose();
const app                   = express();

/*-------------------------------------*/
/*---------------------CONFIG----------*/
/*-------------------------------------*/
const pathToDb = __dirname + '/../lifebox-db/lifebox.db';
const port = 8081;
const publicDir = __dirname + '/public/';

/*-------------------------------------*/
/*---------------FUNCTION MOTEUR-------*/
/*-------------------------------------*/
var addBDD = require('./accessBdd/addBDD');
var getBDD = require('./accessBdd/getBDD');
var modifyBDD = require('./accessBdd/modifyBDD');
var verifyBDD = require('./accessBdd/verifyBDD');

var debugAndSecur = require('./tools/debugAndSecur');

/*-------------------------------------*/
/*---------------SETTING---------------*/
/*-------------------------------------*/
app.set('views', __dirname + '/views/');
app.engine('html', swig.renderFile);
app.set('view engine', 'html');

app.use(express.static('app'));
app.use(express.static(publicDir));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/*-------------------------------------*/
/*---------------ALLOW ACCESS----------*/
/*-------------------------------------*/
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  res.header("Access-Control-Allow-Methods", "GET, POST");
  next();
});

/*-------------------------------------*/
/*--------------ROUTES-----------------*/
/*-------------------------------------*/

/*
*       ADD
*/
app.get('/add-user/:user/:pass', function(req, res){

  try{
    addBDD.addUser(pathToDb, req, res )
  }catch(e){
    addBDD.addError(e)
  }
});

app.get('/add-box/:box/:user/:pass', function(req, res){

  try{
    verifyBDD.verifyUser(pathToDb, req, res, function(pathToDb, req, res){
      addBDD.addBox(pathToDb, req, res )
    }, function(res){
      res.send({'allowed': 0})
    })
  }catch(e){
    addBDD.addError(e)
  }
});

app.get('/add-climat/:name/:user/:pass/:prog/:type/:latLng/:target/:statut', function(req, res){

  try{
    verifyBDD.verifyUser(pathToDb, req, res, function(pathToDb, req, res){
      addBDD.addClimat(pathToDb, req, res )
    }, function(res){
      res.send("modify user forbidden")
    })
  }catch(e){
    addBDD.addError(e)
  }
});

app.get('/add-history/:box/:temp/:hydro/:date', function(req, res){

  try{

      addBDD.addHistory(pathToDb, req, res )

  }catch(e){
    addBDD.addError(e)
  }
});

/*
*       GET
*/
app.get('/get-user/:user/:pass', function(req, res){

  try{
    verifyBDD.verifyUser(pathToDb, req, res, function(pathToDb, req, res){
      getBDD.getUser(pathToDb, req, res )
    }, function(res){
      res.send({'allowed': false})
    })
  }catch(e){
    addBDD.addError(e)
  }
});

app.get('/get-box/:user/:pass', function(req, res){

  try{
    verifyBDD.verifyUser(pathToDb, req, res, function(pathToDb, req, res){
      getBDD.getBox(pathToDb, req, res )
    }, function(res){
      res.send({'allowed': false})
    })
  }catch(e){
    addBDD.addError(e)
  }
});

app.get('/get-climat/:getterClim/:valueClim', function(req, res){

  try{
    getBDD.getClimat(pathToDb, req, res )
  }catch(e){
    addBDD.addError(e)
  }
});

app.get('/get-history/:box/:start/:end', function(req, res){

  try{
    getBDD.getHistory(pathToDb, req, res )
  }catch(e){
    addBDD.addError(e)
  }
});

/*
*       MODIFY
*/

app.get('/modify-user/:user/:pass/:prop/:newValue', function(req, res){

  try{
    verifyBDD.verifyUser(pathToDb, req, res, function(pathToDb, req, res){
      modifyBDD.modifyUser(pathToDb, req, res )
    }, function(res){
     res.send({'allowed': false})
    })
  }catch(e){
    addBDD.addError(e)
  }
});

app.get('/modify-box/:box/:user/:pass/:prop/:newValue', function(req, res){

  try{
    verifyBDD.verifyUser(pathToDb, req, res, function(pathToDb, req, res){
      verifyBDD.verifyBox(pathToDb, req, res, function(pathToDb, req, res){
        modifyBDD.modifyBox(pathToDb, req, res )
      }, function(res){
        res.send({'allowed': false}) /* not user's box */
      })
    }, function(res){
      res.send({'allowed': false}) /* not good user for this box */
    })
  }catch(e){
    addBDD.addError(e)
  }
});

app.get('/modify-climat/:climat/:user/:pass/:prop/:newValue', function(req, res){

  try{
    verifyBDD.verifyUser(pathToDb, req, res, function(pathToDb, req, res){
      verifyBDD.verifyClimat(pathToDb, req, res, function(pathToDb, req, res){
        modifyBDD.modifyClimat(pathToDb, req, res )
      }, function(res){
        res.send({'allowed': false}) /* not your climat */
      })
    }, function(res){
      res.send({'allowed': false}) /* not good user */
    })
  }catch(e){
    addBDD.addError(e)
  }
});

app.get('/modify-history/:box/:user/:pass/:date/:prop/:newValue', function(req, res){

  try{
    verifyBDD.verifyUser(pathToDb, req, res, function(pathToDb, req, res){
      modifyBDD.modifyHistory(pathToDb, req, res)
    }, function(res){
      res.send({'allowed': false})
    })

  }catch(e){
    addBDD.addError(e)
  }
});

/*
*       VERIFY
*/

app.get('/verify-user/:user/:pass', function(req, res){

  try{
    verifyBDD.verifyUser(pathToDb, req, res, function(pathToDb, req, res){
      res.send({'exist': true})
    }, function(res){
      res.send({'exist': false})
    })
  }catch(e){
    addBDD.addError(e)
  }
});

app.get('/verify-box/:box/:user', function(req, res){

  try{
    verifyBDD.verifyBox(pathToDb, req, res, function(pathToDb, req, res){
      res.send({'exist': true})
    }, function(res){
      res.send({'exist': false})
    })
  }catch(e){
    addBDD.addError(e)
  }
});

app.get('/verify-climat/:climat/:user', function(req, res){

  try{
    verifyBDD.verifyClimat(pathToDb, req, res, function(pathToDb, req, res){
      res.send({'exist': true})
    }, function(res){
      res.send({'exist': false})
    })
  }catch(e){
    addBDD.addError(e)
  }
});
/*
*       ADMIN CHECKING
*/

app.get('/4dxm1nl1fe3o0oXx5EcRet/error', function(req, res){

  try{
    debugAndSecur.getError(res)
  }catch(e){
    res.send('impossible de lire le fichier log d\'errreur');
  }
});



/*
*       ALL
*/
app.get('/*', function(req, res){

  try{
    res.send("Welcome hacker ! You are one the centrale server!\r\n We save the ip and addr mac for security of all our user. Thanks to report all strange behavior on the server. report to >naim.aouaichia@gmail.com");
  }catch(e){
    addBDD.addError(e)
  }
});

/*--------------LAUCH------------------*/
app.listen(port);
console.log('Magic happens on port ' + port + ' in the direname =>' + __dirname);
