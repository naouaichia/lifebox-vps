
  /*

  |__ENDPOINT__|

  /getalluser94vg8dezbdgr4e ( GET )
  /user-exist/:email ( GET )

  /user-create/:username/:password/:email ( GET )
  /user-check/:username/:password ( GET )
  /user-update/:username/:password/:keytoupdate/:value ( GET )
  /user-append2field/:username/:password/:field/:data ( GET )
  /sendmdp/:username ( GET )
  /upload/:filename ( POST )

  */

/*-------------------------------------*/
/*--------------------REQUIRE----------*/
/*-------------------------------------*/
  const path       = require('path');        // call express
  const express    = require('express');

  const bodyParser = require('body-parser');
  const liveServer = require("live-server");
  const multer  =   require('multer');
  const swig = require('swig');
  const multipart = require('connect-multiparty'); //for files upload
  const multipartMiddleware = multipart();//for files upload
  const fs = require('fs');
  const lineReader = require('line-reader');
  const sqlite3 = require('sqlite3').verbose();
  const app        = express();
  const mailer = require('express-mailer');

/*-------------------------------------*/
/*---------------------CONFIG----------*/
/*-------------------------------------*/
const pathToDb = __dirname + '/../../lifebox-db/lifebox.db';
//const pathToDb = '/var/www/uzine3d-db/testDB.db';

  var port = 3002;

  var destinationLaData = __dirname + '/public/';

  app.set('views', __dirname + '/views/');
  app.engine('html', swig.renderFile);
  app.set('view engine', 'html');

  app.use(express.static('app'));

  app.use(express.static(destinationLaData));

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  /* ALLOW ALL ORIGIN FOR XHR */
  app.use(function(req, res, next) {
          res.header("Access-Control-Allow-Origin", "*");
          res.header("Access-Control-Allow-Headers", "X-Requested-With");
          res.header("Access-Control-Allow-Headers", "Content-Type");
          res.header("Access-Control-Allow-Methods", "GET, POST");
          next();
  });



/*-------------------------------------*/
/*---------------PART OF CODE----------*/
/*-------------------------------------*/
  // file is included here:
  eval(fs.readFileSync(__dirname+'/app/compEmail.js')+'');
  eval(fs.readFileSync(__dirname+'/app/compSearch.js')+'');
  eval(fs.readFileSync(__dirname+'/app/compTools.js')+'');

  // communication with SQL DB
  eval(fs.readFileSync(__dirname+'/appSQL/sql.js')+'');
  eval(fs.readFileSync(__dirname+'/appSQL/users.js')+'');
  eval(fs.readFileSync(__dirname+'/appSQL/commands.js')+'');


/*-------------------------------------*/
/*----------------MAIL------------------*/
/*-------------------------------------*/

mailer.extend(app, {
  from: 'portefolio.naim@gmail.com',
  host: 'smtp.gmail.com', // hostname
  secureConnection: false, // use SSL
  port: 587, // port for secure SMTP

    requiresAuth: true,
    domains: ["gmail.com", "googlemail.com"],

  transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
  auth: {
    user: 'apigraffiti@gmail.com',
    pass: '9.7y;9Ab8!RsK3'
  }
});

app.get('/sendmdp/:username', function(req, res, next){
  try{
    var username= req.params.username;


     checkUserExist(username,res,function(res,user){
       if(!user){
         res.send({"id":"non"});
       }else{
         console.log("in endpoint");
         console.log(user.email);
         console.log(user.apipassword);
         app.mailer.send('email', {
           to: username, // REQUIRED. This can be a comma delimited string just like a normal email to field.
           subject:"Rappel de vos identifiant de connexion.", // REQUIRED.
           html:emailTempResendMdp(user.email,user.apipassword),
           otherProperty: 'Other Property' // All additional properties are also passed to the template as local variables.
         }, function (err) {
           if (err) {
             // handle error
             console.log(err);
             res.send({"id":"error"});
             return false;
           }
           res.send({"id":"oui"});
         });

       }
     });

  } catch (e) {
    res.send({"id":"non"});
    return false;
  }
});
/*-------------------------------------*/
/*---------UPLOAD FILE SYSTEM----------*/
/*-------------------------------------*/

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/')
  },
  filename: function (req, file, cb) {
    cb(null, req.params.filerename)
  }
})

var upload = multer({ storage: storage });

app.post('/upload/:filerename', upload.any(), function(req, res) {
  res.send({"id":"non"});
});
/*-------------------------------------*/
/*---------------ROUTE-----------------*/
/*-------------------------------------*/
 /*__________ POUR LE DEV ___________*/

 app.get('/getalluser94vg8dezbdgr4e', function(req,res){
   try {
     var db = new sqlite3.Database(pathToDb);

     var result = db.all("SELECT * FROM players",
       function(err, table) {
         res.status(200).send(table);

     });

     db.close();
   } catch (e) {
     res.send({"id":"non"});

   }


 });

 app.get('/user-exist/:email', function(req,res){
   try {
     var email = req.params.email;


     checkUserExist(email,res,function(res,user){
       if(user){
         res.send({"id":"oui"});
       }else{
         res.send({"id":"non"});
       }
     });

     return false;
   } catch (e) {
     res.send({"id":"non"});

   }



 });

  /*---------USER----------*/
  app.get('/user-create/:username/:password/:email', function(req,res){
    try {
      var username = req.params.username;
      var password = req.params.password;
      var email = req.params.email;
      var abonement = req.params.abonement;
      var restriction = req.params.restriction;

      checkUserExist(email,res,function(res,user){
        if(user){
          res.send({"id":"non"});
        }else{
          var avatar = patternAvatar[ getRandomInt(1, 23) ];
          createUser(username,password,email,avatar,abonement,restriction,res,function(){
            res.send({"id":"oui","username":username,"password":password,"email":username,"avatar":avatar});
          });
        }
      });

      return false;
    } catch (e) {
      res.send({"id":"non"});

    }


  });

  app.get('/user-check/:username/:password', function(req,res){
    try {
      var username = req.params.username;
      var password = req.params.password;
      checkUser(username,password,res,function(res,user){
        if(user){
          res.send(JSON.stringify(user));
        }else{
          res.send({"id":"non"});
        }
      });
      return false;
    } catch (e) {
      res.send({"id":"non"});
    }
  });

  app.get('/user-update/:username/:password/:keytoupdate/:value', function(req,res){

    try {
      var username = req.params.username;
      var password = req.params.password;
      var keytoupdate = req.params.keytoupdate;
      var value = req.params.value;
      checkUser(username,password,res,function(res,user){
        if(user){
          updateUser(username, password, keytoupdate, value);
          res.send({"id":"oui"});

        }else{
          res.send({"id":"non"});
        }
      });


      return false;
    } catch (e) {
      res.send({"id":"non"});

    }


  });


  app.get('/user-append2field/:username/:password/:field/:data', function(req,res){
    try {
      var username = req.params.username;
      var password = req.params.password;
      var field = req.params.field;
      var data = req.params.data;
      checkUser(username,password,res,function(res,user){
        if(user){
          append2field(username, password, field, data);
          res.send({"id":"oui"});

        }else{
          res.send({"id":"non"});
        }
      });


      return false;
    } catch (e) {
      res.send({"id":"non"});

    }


  });

  /*---------ALL----------*/
  app.get('/*', function(req,res){

    /*
    var db = openDB(pathToDb);
    db.all('SELECT * FROM commands', function(err, table) {
    res.send(table);
  });
  closeDB(db)
  */
  try {
    res.send({"id":"non"});
    return false;
  } catch (e) {
    res.send({"id":"non"});
    return false;
  }



});

/*-------------------------------------*/
/*--------------LAUCH------------------*/
/*-------------------------------------*/
  app.listen(port);
  console.log('Magic happens on port ' + port + ' in the direname =>' + __dirname);


/*

`id`	TEXT UNIQUE,
	`name`	TEXT UNIQUE,
	`niveau`	INTEGER,
	`permission`	INTEGER,
	`sexe`	INTEGER,
	`accountType`	INTEGER,
	`lat`	REAL,
	`lng`	REAL,
	`email`	TEXT UNIQUE,
	`crew`	TEXT,
	`apipassword`	INTEGER,
	`exp`	INTEGER,
	`features`	TEXT,
	`createdAt`	TEXT,
	`updatedAt`	TEXT,
	`auth0UserId`	TEXT

*/
